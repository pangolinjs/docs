---
home: true
heroImage: /icon.svg
actionText: Get Started →
actionLink: /guide/getting-started.md
footer: Licensed under Hippocratic License 2.1 | Copyright Fynn Becker
features:
- title: Nunjucks
  details: Nunjucks is an HTML templating engine based on JavaScript. Pangolin.js creates static HTML from Nunjucks files.
- title: Sass
  details: Sass is a CSS preprocessor supporting variables, nesting and mixins – among many other features.
- title: JavaScript
  details: JavaScript files are bundled together with webpack and transpiled with Babel and the env preset.
---

<!-- markdownlint-disable MD041 -->

::: warning BETA VERSION
You are currently viewing the docs for the next Pangolin.js version (v6).
:::

## Quick start

Create a new project with `npx` and the Pangolin.js CLI:

```bash
npx @pangolinjs/cli@next create project-name
```
